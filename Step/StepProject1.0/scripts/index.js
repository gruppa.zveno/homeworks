(function ($) {
  $(function () {
    $("ul.tabs-menu").on("click", "li:not(.active)", function () {
      $(this)
        .addClass("active")
        .siblings()
        .removeClass("active")
        .closest("div.tabs")
        .find("div.tabs-items")
        .removeClass("active")
        .eq($(this).index())
        .addClass("active");
    });
  });
})($);

$(document).ready(function () {
  $(".amazing-menu li a").click(function (event) {
    event.preventDefault();

    $(".amazing-menu li").removeClass("selected");
    $(this).parent("li").addClass("selected");

    let imgWidth = "280px";
    let imgHeight = "210px";
    let thisItem = $(this).attr("rel");

    if (thisItem !== "all") {
      $(".amazing-items li[rel=" + thisItem + "]")
        .stop()
        .animate({
          width: imgWidth,
          height: imgHeight,
          opacity: 1,
          marginRight: 0,
          marginLeft: 0,
        });

      $(".amazing-items li[rel!=" + thisItem + "]")
        .stop()
        .animate({
          width: 0,
          height: imgHeight,
          opacity: 1,
          marginRight: 0,
          marginLeft: 0,
        });
    } else {
      $(".amazing-items li").stop().animate({
        opacity: 1,
        height: imgHeight,
        marginRight: 0,
        marginLeft: 0,
        width: imgWidth,
      });
    }
  });

  $(".item li img")
    .animate({ opacity: 1 })
    .hover(
      function () {
        $(this).animate({ opacity: 1 });
      },
      function () {
        $(this).animate({ opacity: 1 });
      }
    );
  $(".slick-container").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    dots: false,
    arrows: false,
    speed: 300,
    adaptiveHeight: true,
  });
  $(".carousel-photo").slick({
    dots: false,
    centerMode: false,
    // centerPadding: "10px",
    arrows: true,
    prevArrow: ".arrow-left",
    nextArrow: ".arrow-right",
    infinite: true,
    speed: 300,
    slidesToScroll: 1,
    adaptiveHeight: true,
    adaptiveWidth: true,
    asNavFor: ".slick-container",
    focusOnSelect: true,
    draggable: false,
    variableHeight: true,
    variableWidth: true,
  });
});

function createNewImg() {
  $(".load-more-btn").remove();
  setTimeout(getMoreImgs(), 2000);
  function getMoreImgs() {
    $(".dis-none").removeClass("dis-none");
    $(".item-category-sec").attr("class", "item-category");
    $(".item-category").css("display", "block");
    $(".item-category-sec").removeClass(".item-category-sec");

    $(".our-amazing-content").css("min-height", "850px");
    $(".our-amazing-content").css("height", "auto");
  }
}
$(".load-more-btn").on("click", createNewImg);
